FROM azul/zulu-openjdk-alpine:11-jre

# Download and install packages
RUN apk update
RUN apk upgrade
RUN apk --no-cache add wget ruby-full jq bash curl

RUN mkdir workdir
COPY pipe /workdir

ENTRYPOINT ["/workdir/pipe.sh"]