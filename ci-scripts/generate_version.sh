#!/bin/bash
# VERSION : We can pass this variable either from ./VERSION file or environment variable. Environment variable will take the precedence
# VERSION_FILE : File where you have stored version or want to store. Default location : ./VERSION
# VERSION_OVERRIDE : To stop generating the new version and use the provided version in VERSION_OVERRIDE
# RELEASE_PHASE : To add the extension to the version like alpha, beta . Default value : alpha .So, the VERSION will be 2021.6.0-alpha

export VERSION_FILE="${VERSION_FILE:-./VERSION}"
export RELEASE_PHASE="${RELEASE_PHASE:-alpha}"

generate_version() {
  if [ -z "${VERSION}" ]; then
    if [ -e "${VERSION_FILE}" ]; then
      read -r VERSION_FROM_FILE < "${VERSION_FILE}"
      echo "Read VERSION : $VERSION_FROM_FILE from ${VERSION_FILE} file"
    fi
    if [ -n "$VERSION_FROM_FILE" ]; then
      VERSION=${VERSION_FROM_FILE}
    else
      echo "Please provide Version from either ${VERSION_FILE} file or VERSION environment variable"
      exit 1
    fi
  fi
  echo "PREVIOUS_VERSION : $VERSION"

  # Get PATCH from the VERSION & increasing it by 1
  PATCH=$(echo "${VERSION}" | awk -F"." '{print $4}')
  PATCH=$(echo "${PATCH}" | awk -F"-" '{print $1}')
  PATCH=$((PATCH + 1))

  # Update VERSION by increased PATCH & append with RELEASE_PHASE
  VERSION_GENERATED=$(echo "${VERSION}" | awk -v PATCH_VAR="${PATCH}" 'BEGIN{FS=OFS="."}{$4=PATCH_VAR}1')
  VERSION_GENERATED+="-${RELEASE_PHASE}"
  echo "GENERATED_VERSION : ${VERSION_GENERATED}"
}

if [ -n "${VERSION_OVERRIDE}" ]; then
  echo "Overriding the version with ${VERSION_OVERRIDE}"
  VERSION_GENERATED=${VERSION_OVERRIDE}
else
  generate_version
fi
echo "${VERSION_GENERATED}" > "${VERSION_FILE}"