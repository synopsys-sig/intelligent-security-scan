function ioPrescription() {
    rm -rf prescription.sh
    $(`wget https://raw.githubusercontent.com/synopsys-sig/io-artifacts/${WORKFLOW_ENGINE_VERSION}/prescription.sh`)

    chmod +x prescription.sh
    sed -i -e 's/\r$//' prescription.sh

    echo "SENSITIVE_PACKAGE_PATTERN"
    echo $SENSITIVE_PACKAGE_PATTERN

    # fixme - if something goes wrong here!?
    ./prescription.sh \
        --io.url=${IO_SERVER_URL} \
        --io.token=${IO_ACCESS_TOKEN} \
        --io.manifest.url=${IO_MANIFEST_URL} \
        --manifest.type=${MANIFEST_TYPE} \
        --asset.id=${BITBUCKET_REPO_FULL_NAME} \
        --release.type=${RELEASE_TYPE} \
        --sensitive.package.pattern=${SENSITIVE_PACKAGE_PATTERN} \
        --scm.type="bitbucket" \
        --workflow.url=${WORKFLOW_ENGINE_SERVER_URL} \
        --workflow.version=${WORKFLOW_ENGINE_VERSION} \
        --polaris.project.name=${POLARIS_PROJECT_NAME} \
        --polaris.branch.name=${POLARIS_BRANCH_NAME} \
        --polaris.url=${POLARIS_SERVER_URL} \
        --polaris.token=${POLARIS_ACCESS_TOKEN} \
        --blackduck.project.name=${BLACKDUCK_PROJECT_NAME} \
        --blackduck.url=${BLACKDUCK_SERVER_URL} \
        --blackduck.api.token=${BLACKDUCK_ACCESS_TOKEN} \
        --scm.owner=${BITBUCKET_WORKSPACE} \
        --scm.repo.name=${BITBUCKET_REPO_SLUG} \
        --scm.branch.name=${BITBUCKET_BRANCH} \
        --bitbucket.commit.id=${BITBUCKET_COMMIT} \
        --bitbucket.username=${BITBUCKET_USERNAME} \
        --bitbucket.password=${BITBUCKET_ACCESS_TOKEN} \
        --jira.api.url=${JIRA_API_URL} \
        --jira.project.name=${JIRA_PROJECT_NAME} \
        --jira.issues.query=${JIRA_ISSUES_QUERY} \
        --jira.username=${JIRA_USERNAME} \
        --jira.auth.token=${JIRA_AUTH_TOKEN} \
        --jira.assignee=${JIRA_ASSIGNEE} \
        --jira.enable=${ENABLE_JIRA} \
        --slack.channel.id=${SLACK_CHANNEL_ID} \
        --slack.token=${SLACK_TOKEN} \
        --stage=${STAGE} \
        --persona=${PERSONA} \
        --IS_SAST_ENABLED=false \
        --IS_SCA_ENABLED=false \
        --IS_DAST_ENABLED=false \
        ${ADDITIONALARGS}

    IS_SAST_ENABLED=$(jq -r '.security.activities.sast.enabled // false' result.json)
    IS_SASTPLUSM_ENABLED=$(jq -r '.security.activities.sastplusm.enabled // false' result.json)
    IS_SCA_ENABLED=$(jq -r '.security.activities.sca.enabled // false' result.json)
    IS_DAST_ENABLED=$(jq -r '.security.activities.dast.enabled // false' result.json)
    IS_DASTPLUSM_ENABLED=$(jq -r '.security.activities.dastplusm.enabled // false' result.json)
    IS_THREATMODEL_ENABLED=$(jq -r '.security.activities.THREATMODEL.enabled // false' result.json)
    IS_NETWORK_ENABLED=$(jq -r '.security.activities.NETWORK.enabled // false' result.json)
    IS_CLOUD_ENABLED=$(jq -r '.security.activities.CLOUD.enabled // false' result.json)
    IS_INFRA_ENABLED=$(jq -r '.security.activities.INFRA.enabled // false' result.json)
    IS_IMAGESCAN_ENABLED=$(jq -r '.security.activities.imageScan.enabled // false' result.json)

    IS_SAST_ENABLED_LN=$(jq -r '.security.activities.sast.longName // "Static Scan"' result.json)
    IS_SASTPLUSM_ENABLED_LN=$(jq -r '.security.activities.sastplusm.longName // "Manual Code Review"' result.json)
    IS_SCA_ENABLED_LN=$(jq -r '.security.activities.sca.longName // "Composition Scan"' result.json)
    IS_DAST_ENABLED_LN=$(jq -r '.security.activities.dast.longName // "Dynamic Scan"' result.json)
    IS_DASTPLUSM_ENABLED_LN=$(jq -r '.security.activities.dastplusm.longName // "Penetration Test"' result.json)
    IS_THREATMODEL_ENABLED_LN=$(jq -r '.security.activities.THREATMODEL.longName // "Threat Model"' result.json)
    IS_NETWORK_ENABLED_LN=$(jq -r '.security.activities.NETWORK.longName // "Network Test"' result.json)
    IS_CLOUD_ENABLED_LN=$(jq -r '.security.activities.CLOUD.longName // "Cloud Configuration Review"' result.json)
    IS_INFRA_ENABLED_LN=$(jq -r '.security.activities.INFRA.longName // "Infrastructure Review"' result.json)
    IS_IMAGESCAN_ENABLED_LN=$(jq -r '.security.activities.imageScan.longName // "Container Scan"' result.json)
    RUN_ID=$(jq -r '.runId // ""' result.json)

    echo -e "\n================================== IO Prescription ======================================="
    echo "Is ${IS_SAST_ENABLED_LN}(SAST) Enabled - ${IS_SAST_ENABLED}"
    echo "Is ${IS_SCA_ENABLED_LN}(SCA) Enabled - ${IS_SCA_ENABLED}"
    echo "Is ${IS_IMAGESCAN_ENABLED_LN}(IMAGESCAN) Enabled - ${IS_IMAGESCAN_ENABLED}"
    echo "Is ${IS_SASTPLUSM_ENABLED_LN}(SASTPLUSM) Enabled - ${IS_SASTPLUSM_ENABLED}"
    echo "Is ${IS_DAST_ENABLED_LN}(DAST) Enabled - ${IS_DAST_ENABLED}"
    echo "Is ${IS_DASTPLUSM_ENABLED_LN}(DASTPLUSM) Enabled - ${IS_DASTPLUSM_ENABLED}"
    echo "Is ${IS_CLOUD_ENABLED_LN}(CLOUD) Enabled - ${IS_CLOUD_ENABLED}"
    echo "Is ${IS_THREATMODEL_ENABLED_LN}(THREATMODEL) Enabled - ${IS_THREATMODEL_ENABLED}"
    echo "Is ${IS_INFRA_ENABLED_LN}(INFRA) Enabled - ${IS_INFRA_ENABLED}"
    echo "Is ${IS_NETWORK_ENABLED_LN}(NETWORK) Enabled - ${IS_NETWORK_ENABLED}"

    if [ $PERSONA == "devsecops" ]; then
        echo "==================================== IO Risk Score ======================================="
        echo "Business Criticality Score - $(jq -r '.riskScoreCard.bizCriticalityScore' result.json)"
        echo "Data Class Score - $(jq -r '.riskScoreCard.dataClassScore' result.json)"
        echo "Access Score - $(jq -r '.riskScoreCard.accessScore' result.json)"
        echo "Open Vulnerability Score - $(jq -r '.riskScoreCard.openVulnScore' result.json)"
        echo "Change Significance Score - $(jq -r '.riskScoreCard.changeSignificanceScore' result.json)"
        export bizScore=$(jq -r '.riskScoreCard.bizCriticalityScore' result.json | cut -d'/' -f2)
        export dataScore=$(jq -r '.riskScoreCard.dataClassScore' result.json | cut -d'/' -f2)
        export accessScore=$(jq -r '.riskScoreCard.accessScore' result.json | cut -d'/' -f2)
        export vulnScore=$(jq -r '.riskScoreCard.openVulnScore' result.json | cut -d'/' -f2)
        export changeScore=$(jq -r '.riskScoreCard.changeSignificanceScore' result.json | cut -d'/' -f2)
        echo -n "Total Score - " && echo "$bizScore + $dataScore + $accessScore + $vulnScore + $changeScore" | bc
    fi

    echo "IS_SAST_ENABLED=${IS_SAST_ENABLED}" >> pipe.meta.env
    echo "IS_SASTPLUSM_ENABLED=${IS_SASTPLUSM_ENABLED}" >> pipe.meta.env
    echo "IS_SCA_ENABLED=${IS_SCA_ENABLED}" >> pipe.meta.env
    echo "IS_DAST_ENABLED=${IS_DAST_ENABLED}" >> pipe.meta.env
    echo "IS_DASTPLUSM_ENABLED=${IS_DASTPLUSM_ENABLED}" >> pipe.meta.env
    echo "IS_THREATMODEL_ENABLED=${IS_THREATMODEL_ENABLED}" >> pipe.meta.env
    echo "IS_NETWORK_ENABLED=${IS_NETWORK_ENABLED}" >> pipe.meta.env
    echo "IS_CLOUD_ENABLED=${IS_CLOUD_ENABLED}" >> pipe.meta.env
    echo "IS_INFRA_ENABLED=${IS_INFRA_ENABLED}" >> pipe.meta.env
    echo "IS_IMAGESCAN_ENABLED=${IS_IMAGESCAN_ENABLED}" >> pipe.meta.env
    echo "RUN_ID=${RUN_ID}" >> pipe.meta.env
    rm -rf ${configFile}
    rm -rf data.json
}