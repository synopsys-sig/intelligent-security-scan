function preValidation() {
    STAGE=${STAGE:?'STAGE variable missing.'};
    MANIFEST_TYPE=${MANIFEST_TYPE:="yml"}
    IO_SERVER_URL=${IO_SERVER_URL:="http://$BITBUCKET_DOCKER_HOST_INTERNAL:9090"};

    if [[ "$IO_SERVER_URL" == "http://$BITBUCKET_DOCKER_HOST_INTERNAL:9090" ]]; then
        sleep 30 # need to wait for io container to spin up
        # health check - todo: improve wait time logic
        healthCheck=$(curl $IO_SERVER_URL/actuator/health)
        echo "Authenticating the Ephemeral IO Server"
        curl $IO_SERVER_URL/api/onboarding/onboard-requests -H "Content-Type:application/vnd.synopsys.io.onboard-request-2+json" -d '{"user":{"username": "ephemeraluser", "password": "P@ssw0rd!", "name":"ephemeraluser", "email":"user@ephemeral.com"}}'
        curl -D cookie.txt $IO_SERVER_URL/api/auth/login -H "Content-Type: application/json" -d '{"loginId": "ephemeraluser","password": "P@ssw0rd!"}'
        sed -n 's/.*access_token*= *//p' cookie.txt > line.txt
        access_token=$(sed 's/;.*//' line.txt)
        curl $IO_SERVER_URL/api/auth/tokens -H "Authorization: Bearer ${access_token}" -H "Content-Type: application/json" -o output.json -d '{"name": "ephemeral-token"}'
        IO_ACCESS_TOKEN=$(jq -r '.token' output.json)
        echo "Ephemeral IO Server Authentication Completed"
        rm -rf cookie.txt
        rm -rf line.txt
        rm -rf output.json
    fi

    IO_ACCESS_TOKEN=${IO_ACCESS_TOKEN:?'IO_ACCESS_TOKEN variable missing.'};
    WORKFLOW_ENGINE_SERVER_URL=${WORKFLOW_ENGINE_SERVER_URL:="http://$BITBUCKET_DOCKER_HOST_INTERNAL:9091"};
    WORKFLOW_ENGINE_VERSION=${WORKFLOW_ENGINE_VERSION:="2023.3.2"};
    JIRA_API_URL=${JIRA_API_URL:="<<JIRA_API_URL>>"};
    JIRA_PROJECT_NAME=${JIRA_PROJECT_NAME:="<<JIRA_PROJECT_NAME>>"};
    JIRA_ISSUES_QUERY=${JIRA_ISSUES_QUERY:="<<JIRA_ISSUES_QUERY>>"};
    JIRA_USERNAME=${JIRA_USERNAME:="<<JIRA_USERNAME>>"};
    JIRA_AUTH_TOKEN=${JIRA_AUTH_TOKEN:="<<JIRA_AUTH_TOKEN>>"};
    JIRA_ASSIGNEE=${JIRA_ASSIGNEE:="<<JIRA_ASSIGNEE>>"};

    if [[ "$MANIFEST_TYPE" == "yml" ]]; then
        configFile="synopsys-io.yml"
    elif [[ "$MANIFEST_TYPE" == "json" ]]; then
        configFile="synopsys-io.json"
    fi

    ADDITIONALARGS=${ADDITIONALARGS:=""};
}
